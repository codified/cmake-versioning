# CMake Versioning System

This is a versioning system for cmake, it will read a file "VERSION" at the root of your project, PROJECT_SOURCE_DIR, take three "." separated value and split them into major, minor and build version.
It will write this to a file version.h in ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME}.

It uses a python script to increment the build number every time the project is built.

Also included is a "cmake/git.cmake", this will read various parameters from the repository such as current repo, git hash, branch. It stores these in variables which can be used with cmake's "configure_file" to enhance your program's build information.

## Requirements

* cmake
* python
* git

## Repository

The main repository for this code is [git.ignifi.me](https://git.ignifi.me/clu/cmake-versioning)

